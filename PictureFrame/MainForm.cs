﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace PictureFrame {
	public partial class MainForm : Form {
		Random Random = new Random();
		List<string> Files = new List<string>();

		public MainForm() {
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e) {
			var bounds = Properties.Settings.Default.WindowSize;

			bool[] bounds_on_screen = { false, false, false, false };
			foreach (var screen in Screen.AllScreens) {
				if (bounds.Left >= screen.Bounds.Left)
					bounds_on_screen[0] = true;

				if (bounds.Top >= screen.Bounds.Top)
					bounds_on_screen[1] = true;

				if (bounds.Right <= screen.Bounds.Right)
					bounds_on_screen[2] = true;

				if (bounds.Bottom <= screen.Bounds.Bottom)
					bounds_on_screen[3] = true;
			}

			if (bounds_on_screen[0] &&
				bounds_on_screen[1] &&
				bounds_on_screen[2] &&
				bounds_on_screen[3]) {
				Bounds = bounds;
			}

			// From minutes to milliseconds
			timer.Interval = (int)(1000.0f * 60.0f * Properties.Settings.Default.Interval);

			LoadFiles();
		}

		private void timer_Tick(object sender, EventArgs e) {
			Properties.Settings.Default.PictureIndex++;
			ShowPicture();
			Properties.Settings.Default.Save();
		}

		private void ShowPicture() {
			if (Files.Count == 0)
				return;

			if (Properties.Settings.Default.PictureIndex >= Files.Count) {
				Properties.Settings.Default.RandomSeed = new Random().Next();
				Properties.Settings.Default.PictureIndex = 0;
			}

			if (Properties.Settings.Default.PictureIndex < 0) {
				Properties.Settings.Default.PictureIndex = Files.Count - 1;
			}

			pictureBox.ImageLocation = Files[Properties.Settings.Default.PictureIndex];
			Properties.Settings.Default.Save();
		}

		private void closeToolStripMenuItem_Click(object sender, EventArgs e) {
			Close();
		}

		private void settingsToolStripMenuItem_Click(object sender, EventArgs e) {
			if (new SettingsForm().ShowDialog() == DialogResult.OK) {
				timer.Interval = (int)(1000.0f * 60.0f * Properties.Settings.Default.Interval);
				timer.Start();
				LoadFiles();
			}
		}

		void LoadFiles() {
			Files.Clear();

			if (Properties.Settings.Default.Folders != null)
				foreach (var folder in Properties.Settings.Default.Folders)
					Files.AddRange(Directory.GetFiles(folder, "*.jpg", SearchOption.AllDirectories));

			if (Properties.Settings.Default.Shuffle)
				Shuffle(ref Files);

			ShowPicture();
		}

		private void MainForm_ResizeEnd(object sender, EventArgs e) {
			Properties.Settings.Default.WindowSize = Bounds;
			Properties.Settings.Default.Save();
		}

		private void nextToolStripMenuItem_Click(object sender, EventArgs e) {
			NextPicture();
		}

		private void previousToolStripMenuItem_Click(object sender, EventArgs e) {
			PreviousPicture();
		}

		void Shuffle<T>(ref List<T> list) {
			Random random = new Random(Properties.Settings.Default.RandomSeed);

			for (int i = list.Count; i > 1; --i) {
				int j = random.Next(i);
				T temp = list[j];
				list[j] = list[i - 1];
				list[i - 1] = temp;
			}
		}

		void NextPicture() {
			Properties.Settings.Default.PictureIndex++;
			ShowPicture();
		}

		void PreviousPicture() {
			if (!Properties.Settings.Default.Shuffle || Properties.Settings.Default.PictureIndex > 0) {
				Properties.Settings.Default.PictureIndex--;
				ShowPicture();
			}
		}

		private void MainForm_KeyDown(object sender, KeyEventArgs e) {
			if (e.KeyCode == Keys.Right)
				NextPicture();

			if (e.KeyCode == Keys.Left)
				PreviousPicture();
		}
	}
}
