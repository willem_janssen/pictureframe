﻿namespace PictureFrame {
	partial class SettingsForm {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.intervalUpDown = new System.Windows.Forms.NumericUpDown();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.folderBox = new System.Windows.Forms.ListBox();
			this.addFolderButton = new System.Windows.Forms.Button();
			this.removeFolderButton = new System.Windows.Forms.Button();
			this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
			this.label3 = new System.Windows.Forms.Label();
			this.shuffleBox = new System.Windows.Forms.CheckBox();
			this.pictureIndexUpDown = new System.Windows.Forms.NumericUpDown();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.intervalUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureIndexUpDown)).BeginInit();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.button1.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.button1.Location = new System.Drawing.Point(276, 250);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 0;
			this.button1.Text = "OK";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.button2.Location = new System.Drawing.Point(357, 250);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(75, 23);
			this.button2.TabIndex = 1;
			this.button2.Text = "Cancel";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// intervalUpDown
			// 
			this.intervalUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.intervalUpDown.Location = new System.Drawing.Point(313, 31);
			this.intervalUpDown.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
			this.intervalUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.intervalUpDown.Name = "intervalUpDown";
			this.intervalUpDown.Size = new System.Drawing.Size(79, 20);
			this.intervalUpDown.TabIndex = 3;
			this.intervalUpDown.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(398, 31);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(43, 13);
			this.label1.TabIndex = 4;
			this.label1.Text = "minutes";
			// 
			// label2
			// 
			this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(195, 31);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(114, 13);
			this.label2.TabIndex = 5;
			this.label2.Text = "Time between pictures";
			this.label2.Click += new System.EventHandler(this.label2_Click);
			// 
			// folderBox
			// 
			this.folderBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.folderBox.FormattingEnabled = true;
			this.folderBox.Location = new System.Drawing.Point(12, 31);
			this.folderBox.Name = "folderBox";
			this.folderBox.Size = new System.Drawing.Size(157, 173);
			this.folderBox.TabIndex = 6;
			// 
			// addFolderButton
			// 
			this.addFolderButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.addFolderButton.Location = new System.Drawing.Point(12, 218);
			this.addFolderButton.Name = "addFolderButton";
			this.addFolderButton.Size = new System.Drawing.Size(75, 23);
			this.addFolderButton.TabIndex = 7;
			this.addFolderButton.Text = "Add";
			this.addFolderButton.UseVisualStyleBackColor = true;
			this.addFolderButton.Click += new System.EventHandler(this.addFolderButton_Click);
			// 
			// removeFolderButton
			// 
			this.removeFolderButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.removeFolderButton.Location = new System.Drawing.Point(94, 218);
			this.removeFolderButton.Name = "removeFolderButton";
			this.removeFolderButton.Size = new System.Drawing.Size(75, 23);
			this.removeFolderButton.TabIndex = 8;
			this.removeFolderButton.Text = "Remove";
			this.removeFolderButton.UseVisualStyleBackColor = true;
			this.removeFolderButton.Click += new System.EventHandler(this.removeFolderButton_Click);
			// 
			// folderBrowserDialog
			// 
			this.folderBrowserDialog.ShowNewFolderButton = false;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(9, 9);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(74, 13);
			this.label3.TabIndex = 9;
			this.label3.Text = "Picture folders";
			// 
			// shuffleBox
			// 
			this.shuffleBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.shuffleBox.AutoSize = true;
			this.shuffleBox.Location = new System.Drawing.Point(313, 57);
			this.shuffleBox.Name = "shuffleBox";
			this.shuffleBox.Size = new System.Drawing.Size(15, 14);
			this.shuffleBox.TabIndex = 10;
			this.shuffleBox.UseVisualStyleBackColor = true;
			// 
			// pictureIndexUpDown
			// 
			this.pictureIndexUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.pictureIndexUpDown.Location = new System.Drawing.Point(313, 77);
			this.pictureIndexUpDown.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
			this.pictureIndexUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.pictureIndexUpDown.Name = "pictureIndexUpDown";
			this.pictureIndexUpDown.Size = new System.Drawing.Size(79, 20);
			this.pictureIndexUpDown.TabIndex = 11;
			this.pictureIndexUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
			// 
			// label4
			// 
			this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(195, 57);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(40, 13);
			this.label4.TabIndex = 12;
			this.label4.Text = "Shuffle";
			// 
			// label5
			// 
			this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(195, 79);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(104, 13);
			this.label5.TabIndex = 13;
			this.label5.Text = "Current picture index";
			// 
			// SettingsForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(444, 285);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.pictureIndexUpDown);
			this.Controls.Add(this.shuffleBox);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.removeFolderButton);
			this.Controls.Add(this.addFolderButton);
			this.Controls.Add(this.folderBox);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.intervalUpDown);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.MaximizeBox = false;
			this.Name = "SettingsForm";
			this.Text = "Picture frame settings";
			this.Load += new System.EventHandler(this.SettingsForm_Load);
			((System.ComponentModel.ISupportInitialize)(this.intervalUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureIndexUpDown)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.NumericUpDown intervalUpDown;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ListBox folderBox;
		private System.Windows.Forms.Button addFolderButton;
		private System.Windows.Forms.Button removeFolderButton;
		private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.CheckBox shuffleBox;
		private System.Windows.Forms.NumericUpDown pictureIndexUpDown;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
	}
}