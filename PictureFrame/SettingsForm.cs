﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PictureFrame {
	public partial class SettingsForm : Form {
		public SettingsForm() {
			InitializeComponent();
		}

		private void SettingsForm_Load(object sender, EventArgs e) {
			if (Properties.Settings.Default.Folders != null)
				folderBox.Items.AddRange(Properties.Settings.Default.Folders);

			intervalUpDown.Value = (decimal)Properties.Settings.Default.Interval;

			folderBrowserDialog.SelectedPath = Environment.GetFolderPath(Environment.SpecialFolder.MyPictures);

			shuffleBox.Checked = Properties.Settings.Default.Shuffle;

			pictureIndexUpDown.Value = Properties.Settings.Default.PictureIndex + 1;
		}

		private void label2_Click(object sender, EventArgs e) {

		}

		private void button2_Click(object sender, EventArgs e) {
			Close();
		}

		private void button1_Click(object sender, EventArgs e) {
			Properties.Settings.Default.Folders = folderBox.Items.Cast<string>().ToArray();
			Properties.Settings.Default.Interval = (float)intervalUpDown.Value;
			Properties.Settings.Default.Shuffle = shuffleBox.Checked;
			Properties.Settings.Default.PictureIndex = (int)pictureIndexUpDown.Value - 1;
			Properties.Settings.Default.Save();

			Close();
		}

		private void addFolderButton_Click(object sender, EventArgs e) {
			if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
				folderBox.Items.Add(folderBrowserDialog.SelectedPath);
		}

		private void removeFolderButton_Click(object sender, EventArgs e) {
			if (folderBox.SelectedItem != null)
				folderBox.Items.Remove(folderBox.SelectedItem);
		}
	}
}
